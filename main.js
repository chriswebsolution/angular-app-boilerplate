console.log('main is loaded.');
var app = angular.module('app', ['ui.router', 'shoppinpal.mobile-menu']);

app.controller('AppCtrl', function () {
  console.log('AppCtrl is loaded.');
})

.config(function ($stateProvider, $urlRouterProvider) {
  $urlRouterProvider
    .when('', '/home')
    .otherwise('/home');

  $stateProvider
    .state('home', {
      url: '/home',
      templateUrl: 'module/home/home-tmpl.html'
    })

    .state('test1', {
      url: '/test1',
      template: '<h1>test1</h1>'
    })
  ;
});